<?php
session_start();
session_register_shutdown();
require_once ($_SERVER['DOCUMENT_ROOT'] . '/config.php');

function isLoggedIn(){
    return isset($_SESSION['username']);
}

function loginRequired(){
    if(!isLoggedIn()){
        header("Location: /login.php");
        die;
    }
}

function adminRequired(){
    if(!isAdmin()){
        header("Location: /login.php");
        die;
    }
}

function isAdmin(){
    if(isset($_SESSION['userType'])){
        return $_SESSION['userType'] == 'admin';
    }
    return false;
}

function login($username, $password){
    global $db;
    $q = $db->prepare("SELECT o.id, o.username, o.pwd, u.type FROM owners o LEFT JOIN userTypes u ON o.userType = u.id WHERE o.username LIKE ? LIMIT 1");
    if (!$q){
        die("Query was incorrect");
    }
    $q->bind_param("s", $username);
    $q->execute();
    $q->store_result();
    $q->bind_result($userId, $dbUsername, $dbPassword, $dbUserType);
    $q->fetch();
    if(password_verify($password, $dbPassword)){
        $_SESSION['userId'] = $userId;
        $_SESSION["username"] = $dbUsername;
        $_SESSION["userType"] = $dbUserType;
        header("Location: /");
        die;
    }
    else{
        return null;
    }
}

function logout(){
    $_SESSION = array();
    header("Location: /");
    die;
}