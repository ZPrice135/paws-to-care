<?php include './templates/header.php'; ?>

<div class="container">
  <h1 class="display-4">Paws to Care</h1>
  <p class="lead">Always there for you</p>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed ex id odio rutrum feugiat. Nullam sodales, ex nec feugiat dignissim, augue leo elementum est, nec gravida ligula enim eget urna. Vivamus quis hendrerit sapien. Quisque ante nulla, luctus sed mi nec, iaculis tristique est. Vestibulum eu rutrum enim. Sed sit amet vulputate nulla. Suspendisse aliquet tempor vulputate. Aenean sit amet mollis orci, et imperdiet leo. Cras et pulvinar lectus, fermentum tempor elit. Vestibulum maximus turpis nibh, sed tempor neque facilisis fringilla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tortor diam, varius vel vulputate at, venenatis eget augue. Quisque consequat rhoncus orci a vulputate.</p>
  <hr class="my-4">
  <div class="row">
    <div class="col-8">
      <h3>Lorem ipsum dolor sit amet</h3>
      <p>Fusce sagittis pharetra nulla, a commodo eros. Cras fermentum at libero nec sollicitudin. Nulla pharetra est nisi, sit amet interdum sapien ornare ultrices. Nam commodo, neque ac dapibus consequat, tellus ipsum egestas justo, eget cursus mi justo sit amet felis. In eleifend neque quis arcu imperdiet interdum. Mauris iaculis cursus urna id sodales. Nunc et ante eu velit eleifend vehicula. Cras posuere rutrum consequat. Ut pharetra ut justo vitae mollis. Vestibulum suscipit rutrum tortor, non eleifend mauris consequat eget. Integer vitae convallis arcu. Cras diam nibh, faucibus sit amet hendrerit sed, mattis et arcu. Nulla bibendum maximus mi, non pharetra lectus condimentum ac.</p>
      <!--p>Integer semper, neque id feugiat eleifend, enim nisi sodales mi, vitae bibendum lectus lacus in velit. Nulla eu velit eu lorem pretium viverra. Curabitur pretium tellus diam, quis cursus magna pharetra vitae. Integer bibendum lacus a posuere mattis. Praesent pretium est ac nisl convallis gravida. Vestibulum vitae convallis ligula. Nunc tempor rutrum ipsum, id tincidunt tortor faucibus molestie. Proin a tortor libero. Nullam eros turpis, semper nec nulla ac, gravida rutrum erat. Aliquam laoreet scelerisque lectus, at vulputate felis porttitor non. Praesent commodo vulputate augue, sit amet euismod metus auctor a.</p--> 
    </div>
    <div class="col-4">
      <a href="./contact.html" class="text-dark"><h5>Contact Us</h5></a>
      <table class="table">
        <tbody>
          <tr>
            <td class="align-text-top">Phone</td>
            <td class="align-text-top">123-456-7890</td>
          </tr>
          <tr>
            <td class="align-text-top">Email</td>
            <td class="align-text-top">support@pawstocare.com</td>
          </tr>
          <tr>
            <td class="align-text-top">Address</td>
            <td class="align-text-top ">
            123 South 500 West<br>
            Orem, Utah 84058
            </td>
          </tr> 
        </tbody>  
      </table>
    </div>
  </div>        
</div>

<?php include './templates/footer.php'; ?>