<?php 
include "../templates/header.php" ;
require_once "./auth.php";
require_once "../config.php";
require_once '../models/pagination.php';

$limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : 10;
$page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
$modalPage  = ( isset( $_GET['m_page'] ) ) ? $_GET['m_page'] : 1;
$links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 2;
$query      = "SELECT o.id, o.fname, o.lname, o.add1, o.add2, o.city, o.st, o.zip FROM owners o JOIN ownernotes n ON n.ownersFk = o.id";

$Paginator = new Paginator( $db, $query );

$results = $Paginator->getData( $limit, $page );
?>


<div class="container">
    <div class="row">
        <h1 class="display-4">Owners</h1>
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Name" aria-label="Species" name="Species" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Address" aria-label="Address" name="Address" disabled>
                            </div>
                        </th>
                        <th scope="col">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php for( $i = 0; $i < count( $results->data ); $i++ ): ?>
                        <tr>
                            <td><?= $results->data[$i]['fname'] . " " . $results->data[$i]['lname'] ?></td>
                            <td>
                                <?=
                                    $results->data[$i]['add1'] . "<br>" . 
                                    ($results->data[$i]['add2'] ? $results->data[$i]['add2']  . "<br>" : "") .
                                    $results->data[$i]['city'] . ", " . 
                                    $results->data[$i]['st'] . " " . 
                                    $results->data[$i]['zip']
                                ?>
                            </td>
                            <!-- Modal Start -->
                            <td>
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal<?= $results->data[$i]['id'] ?>">Notes</button>
                                <div id="modal<?= $results->data[$i]['id'] ?>" class="modal" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"><?= $results->data[$i]['fname'] . " " . $results->data[$i]['lname'] ?></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="table-responsive">
                                                    <table class="table table-sm">
                                                        <thead>
                                                            <tr>
                                                                <td>
                                                                    Date
                                                                </td>
                                                                <td>
                                                                    Note
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $m_query = $db->prepare("SELECT n.vetName, n.date, n.note FROM ownernotes n LEFT JOIN owners o ON n.ownersFk = o.id WHERE o.id = ?"); 
                                                                $m_query->bind_param("s", $results->data[$i]['id']); 
                                                                $m_query->execute();
                                                                $m_query->bind_result($vetName, $date, $note);
                                                                while($m_query->fetch()) {
                                                                    echo '<tr><td>'.date('Y-m-d', strtotime($date)).'</td><td>'.$note.'</td></tr>';
                                                                }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <!-- Modal End -->
                        </tr> 
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <div class="mb-3">
            <?= $Paginator->createLinks( $links, 'pagination' ); ?>
        </div>
    </div>
</div>

<?php include "../templates/footer.php" ?>