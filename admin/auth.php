<?php
if( !$_SESSION['userId'] &&
    !$_SESSION["username"] &&
    $_SESSION["userType"] !== "admin" )
    {
        header("Location: /");
        die;
    }
?>