<?php 
include "../templates/header.php" ;
require_once "./auth.php";
?>

<div class="container">
    <div class="row">
        <h1 class="display-4">Cats</h1>
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Name" aria-label="Name" name="Name" id="filterName">
                                <div class="input-group-append sortGroup" id="sortName" style="cursor: pointer;">
                                    <button class="btn btn-secondary" type="button" title="sort"><span class="sortBtn invisible">▼</span></button>
                                </div>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Breed" aria-label="Breed" name="Breed" id="filterBreed">
                                <div class="input-group-append sortGroup" id="sortBreed" style="cursor: pointer;">
                                    <button class="btn btn-secondary" type="button" title="sort"><span class="sortBtn invisible">▼</span></button>
                                </div>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Sex" aria-label="Sex" name="Sex" id="Sex" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Shots" aria-label="Shots" name="Shots" id="Shots" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Declawed" aria-label="Declawed" name="Declawed" id="Declawed" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Neutered" aria-label="Neutered" name="Neutered" id="Neutered" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Birthdate" aria-label="Birthdate" name="Birthdate" id="Birthdate" disabled>
                            </div>
                        </th>
                        <th scope="col">

                        </th>
                        <th scope="col">

                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div id="pagination" class="mb-3"></div>
        </div>
    </div>
</div>

<script>

$(document).ready(() => {
    let sortedRow;
    let filters = {};

    let coreData = [];
    let currData = []; 


    //Initial Data Population
    $.getJSON("/models/cats", (cats) => {
        coreData = cats; 
        currData = cats;
        buildRows();
    });

    $("#sortName").click(() => {
        sort("Name");
    });
    $("#sortBreed").click(() => {
        sort("Breed");
    });

    $("#filterName").keypress( _.debounce(() => {
        filters["name"] = $(`#filterName`).val();
        console.log(filters)
        checkFilters();
    }, 200));

    $("#filterBreed").keypress( _.debounce(() => {
        filters["breed"] = $(`#filterBreed`).val();
        checkFilters();
    }, 200));
    

    function buildRows() {
        let presData = [];
        for (let dat of currData) {
            presData.push(
                buildRow(dat)
            );
        }

        $("#pagination").pagination({
            dataSource: presData,
            callback: function(data, pagination) {
                $('tbody').html(data);
            }
        });
    }

    function sort(rowIndex) {
        //Start with coreData & sort on sortCol (index) based on type. 
        if (rowIndex == sortedRow) {
            currData.reverse();
            //This is where you will need to switch to the other value
            $(`#sort${sortedRow} button span`).html(getArrow(sortedRow));
            buildRows();
            return;
        }

        if(rowIndex.toLowerCase() == "name") {
            currData.sort(sortName);
        }
        if(rowIndex.toLowerCase() == "breed") {
            currData.sort(sortBreed);
        }

        sortedRow = rowIndex;
        $(".sortBtn").addClass("invisible")
        $(`#sort${sortedRow} button span`).html(arrowDown).removeClass("invisible");
        buildRows();
    }

    function checkFilters() {
        currData = $.extend(true, [], coreData);

        $.each(filters, (index, value) => {
            if (currData.length <= 0) {
                currData = [];
                buildRows();
                return;
            }
            let tmp = currData.filter((record) => {
                return record[index].toLowerCase().startsWith(value.toLowerCase());
            });
            currData = $.extend([], tmp);
        });
        buildRows();    
    }

    function buildRow(cat) {
        birthDate = new Date(Date.parse(cat['birthday']));
        return (
            `<tr>
                <td>${cat['name']}</td>
                <td>${cat['breed']}</td>
                <td>${cat['sex']}</td>
                <td>${cat['shots'] ? '<i class="fas fa-check" style="color:green;"></i>' : '<i class="fas fa-times"></i>'}</td>
                <td>${cat['declawed'] ? '<i class="fas fa-check"  style="color:green;"></i>' : '<i class="fas fa-times"></i>'}</td>
                <td>${cat['neutered'] ? '<i class="fas fa-check"  style="color:green;"></i>' : '<i class="fas fa-times"></i>'}</td>
                <td>${birthDate.getUTCMonth()+1}/${birthDate.getUTCDate()}/${birthDate.getUTCFullYear()}</td>
                <td>
                    <button class="btn btn-secondary" data-toggle="modal" data-target="#notes${cat['id']}">Notes</button>
                    ${noteModal(cat)}
                </td>

                <td>
                    <button class="btn btn-secondary" data-toggle="modal" data-target="#owners${cat['id']}">Owners</button>
                    ${ownerModal(cat)}
                </td>
            </tr>`
        );
    }

    function noteModal(cat) {
        return (
            `<div class="modal" id="notes${cat['id']}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">${cat["name"]}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div> `
        );
    }

    function ownerModal(cat) {
        return (
            `<div class="modal" id="owners${cat['id']}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">${cat["name"]}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Notes?                     
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                </div>
            </div> `
        );
    }
});
</script>



<?php include "../templates/footer.php" ?>