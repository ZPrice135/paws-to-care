<?php 
include "../templates/header.php" ;
require_once "./auth.php";
?>

<div class="container">
    <div class="row">
        <h1 class="display-4">Dogs</h1>
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Name" aria-label="Name" name="Name" id="filterName">
                                <div class="input-group-append sortGroup" id="sortName" style="cursor: pointer;">
                                    <button class="btn btn-secondary" type="button" title="sort"><span class="sortBtn invisible">▼</span></button>
                                </div>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Breed" aria-label="Breed" name="Breed" id="filterBreed">
                                <div class="input-group-append sortGroup" id="sortBreed" style="cursor: pointer;">
                                    <button class="btn btn-secondary" type="button" title="sort"><span class="sortBtn invisible">▼</span></button>
                                </div>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Sex" aria-label="Sex" name="Sex" id="Sex" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Shots" aria-label="Shots" name="Shots" id="Shots" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Size" aria-label="Size" name="Size" id="Size" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Neutered" aria-label="Neutered" name="Neutered" id="Neutered" disabled>
                            </div>
                        </th>
                        <th scope="col">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Birthdate" aria-label="Birthdate" name="Birthdate" id="Birthdate" disabled>
                            </div>
                        </th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div id="pagination" class="mb-3"></div>
        </div>
    </div>
</div>

<script>

$(document).ready(() => {
    let sortedRow;
    let filters = {};

    let coreData = [];
    let currData = []; 


    //Initial Data Population
    $.getJSON("/models/dogs", (dogs) => {
        coreData = dogs; 
        currData = dogs;
        buildRows();
    });

    $("#sortName").click(() => {
        sort("Name");
    });
    $("#sortBreed").click(() => {
        sort("Breed");
    });

    $("#filterName").keypress( _.debounce(() => {
        filters["name"] = $(`#filterName`).val();
        checkFilters();
    }, 200));

    $("#filterBreed").keypress( _.debounce(() => {
        filters["breed"] = $(`#filterBreed`).val();
        checkFilters();
    }, 200));
    

    function buildRows() {
        let presData = [];
        for (let dat of currData) {
            presData.push(
                buildRow(dat)
            );
        }

        $("#pagination").pagination({
            dataSource: presData,
            callback: function(data, pagination) {
                $('tbody').html(data);
            }
        });
    }

    function sort(rowIndex) {
        //Start with coreData & sort on sortCol (index) based on type. 
        if (rowIndex == sortedRow) {
            currData.reverse();
            //This is where you will need to switch to the other value
            $(`#sort${sortedRow} button span`).html(getArrow(sortedRow));
            buildRows();
            return;
        }

        if(rowIndex.toLowerCase() == "name") {
            currData.sort(sortName);
        }
        if(rowIndex.toLowerCase() == "breed") {
            currData.sort(sortBreed);
        }
        if(rowIndex.toLowerCase() == "species") {
            currData.sort(sortSpecies)
        }

        sortedRow = rowIndex;
        $(".sortBtn").addClass("invisible")
        $(`#sort${sortedRow} button span`).html(arrowDown).removeClass("invisible");
        buildRows();
    }

    function checkFilters() {
        currData = $.extend(true, [], coreData);

        $.each(filters, (index, value) => {
            if (currData.length <= 0) {
                currData = [];
                buildRows();
                return;
            }
            let tmp = currData.filter((record) => {
                return record[index].toLowerCase().startsWith(value.toLowerCase());
            });
            currData = $.extend([], tmp);
        });
        buildRows();    
    }

    function buildRow(val) {
        birthDate = new Date(Date.parse(val['birthdate']));
        size = getSizeTransform(val['size']);
        return (
            `<tr>
                <td>${val['name']}</td>
                <td>${val['breed']}</td>
                <td>${val['sex']}</td>
                <td>${val['shots'] ? '<i class="fas fa-check" style="color:green;"></i>' : '<i class="fas fa-times"></i>'}</td>
                <td>${size}</td>
                <td>${val['neutered'] ? '<i class="fas fa-check"  style="color:green;"></i>' : '<i class="fas fa-times"></i>'}</td>
                <td>${birthDate.getUTCMonth()+1}/${birthDate.getUTCDate()}/${birthDate.getUTCFullYear()}</td>
                <td>
                    <button class="btn btn-secondary" data-toggle="modal" data-target="#notes${val['id']}">Notes</button>
                    ${noteModal(val)}
                </td>

                <td>
                    <button class="btn btn-secondary" data-toggle="modal" data-target="#owners${val['id']}">Owners</button>
                    ${ownerModal(val)}
                </td>
            </tr>`
        );
    }
});
</script>

<?php include "../templates/footer.php" ?>