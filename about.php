<?php include './templates/header.php'; ?>

<div class="container">
  <h1 class="display-4">About Us</h1>
  <p class="lead">Meet the Paws to Care team</p>
  <div class="row">
    <div class="col-lg-3 col-md-5 mb-3">
      <div class="card">
        <img class="card-img-top" src="./assets/placeholder-600sq.bmp" alt="Card image cap">
        <div class="card-body">
          <h3 class="card-title">Cynthia Truska</h5>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed ex id odio rutrum feugiat.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 offset-lg-1 col-md-5 offset-md-2 mb-3">
      <div class="card">
        <img class="card-img-top" src="./assets/placeholder-600sq.bmp" alt="Card image cap">
        <div class="card-body">
          <h3 class="card-title">Frank Truska</h5>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed ex id odio rutrum feugiat.</p>
        </div>
      </div>  
    </div>
    <div class="col-lg-3 offset-lg-1 col-md-5 offset-md-2 mb-3">
      <div class="card">
        <img class="card-img-top" src="./assets/placeholder-600sq.bmp" alt="Card image cap">
        <div class="card-body">
          <h3 class="card-title">Dr. Jose Gonzales Sr.</h5>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed ex id odio rutrum feugiat.</p>
        </div>
      </div>
    </div>
  </div>        
</div>

<?php include './templates/footer.php'; ?>