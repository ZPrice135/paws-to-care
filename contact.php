<?php include './templates/header.php'; ?>

<div class="container">
  <h1 class="display-4">Contact Us</h1>
  <p class="lead">Please call us for more information</p>
  <div class="row">
    <div class="col-12">
      <table class="table">
        <tbody>
          <tr>
            <td class="align-text-top"><b>Phone</b></td>
            <td class="align-text-top"><a class="text-dark" href="tel:123-456-7890">123-456-7890</a></td>
          </tr>
          <tr>
            <td><b>Hours</b></td>
            <td>
            8:00 AM - 5:00 PM<br>
            Monday - Saturday
            </td>
          </tr>
          <tr>
            <td class="align-text-top"><b>Email</b></td>
            <td class="align-text-top"><a class="text-dark" href="mailto:support@pawstocare.com">support@pawstocare.com</a></td>
          </tr>
          <tr>
            <td class="align-text-top"><b>Address</b></td>
            <td class="align-text-top ">
            123 South 500 West<br>
            Orem, Utah 84058
            </td>
          </tr> 
        </tbody>  
      </table>
    </div>
  </div>        
</div>
<?php include './templates/footer.php'; ?>