const arrowDown = "▼";
const arrowUp = "▲";

function getArrow(row) {
    let sortedRow = $(`#sort${row} button span`).html();
    return ( sortedRow == arrowDown ) ? arrowUp : arrowDown;
}

function sortString(a, b) {
    a = a.toLowerCase();
    b = b.toLowerCase();
    if (a < b) return -1;
    if (a > b) return 1;
    return 0
}

function sortName(a, b) {
    return sortString(a['name'], b['name'] );
}

function sortBreed(a, b) {
    return sortString(a['breed'], b['breed'] );
}

function sortSpecies(a, b) {
    return sortString(a['species'], b['species'] );
}

function getSizeTransform(value) {
    if(!value || value <= 0) {
        console.error("Size must exis and be greater than 0");
        return;
    }
    if (value < 20) return "Small";
    if (value < 50) return "Medium";
    if (value < 100) return "Large"; 
    return "Giant";
}

function noteModal(val) {
  return (
        `<div class="modal" id="notes${val['id']}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">${val["name"]}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Unfortunately, Incomplete.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> `
  );
}

function ownerModal(val) {
  return (
        `<div class="modal" id="owners${val['id']}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">${val["name"]}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Also Incomplete.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div> `
  );
}