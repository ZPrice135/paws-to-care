 /*  var randomName = faker.name.findName(); // Caitlyn Kerluke
  var randomEmail = faker.internet.email(); // Rusty@arne.info
  var randomCard = faker.helpers.createCard(); // random contact card containing many properties
 */

const MAX_OWNERS = 2; //(This will always be one greater then stated, see 'getOwnerCount()')
const MAX_DOG_AGE = 16; 
const MAX_DOG_SIZE = 150;
const MAX_CAT_AGE = 14;
const MAX_EXOTIC_AGE = 70;

const getAnimal = (num, arr) => arr[num % arr.length].slice(0, 1).toUpperCase() + arr[num % arr.length].slice(1).toLowerCase();

const getOwnerCount = (val) => (val % MAX_OWNERS) + 1;

function getGender(val) {
    val = val % 3;
    if (val == 0) return "U"; 
    if (val == 1) return "F";
    return "M"
}

let dogs = []
// Create Dogs 
for (let i = 0; i < 1000; i = i + 1)  {
    let dog = {};
    dog.Name = faker.name.firstName();
    dog.Breed = getAnimal(faker.random.number(), DOG_BREEDS); //Create a list, then % on list.length to get which one
    dog.Sex = getGender(faker.random.number()); //Decide on which each value is

    dog.Shots = faker.random.boolean();
    
    dog.birthDate = faker.date.recent(); //Typically dogs will live up to 16 years (small dogs at least)
    dog.Size = faker.random.number() % MAX_DOG_SIZE; //Check this max age 

    dog.Liecensed = faker.random.boolean() ? faker.date.future() : faker.date.recent();

    dog.Neutered = faker.random.boolean();

    let ownerNum = getOwnerCount(faker.random.number()); 
    let owners = [];
    for (let i = 0; i < ownerNum; i++) {
        owners.push(faker.random.number() % 1000);
    }
    dog.ownerIds = owners;

    let noteNum = (faker.random.number() % 10); 
    notes = [];
    for (let i = 0; i < noteNum; i++) {
        notes.push(faker.lorem.text());
    }
    dog.NoteIds = notes
    dogs.push(dog);
}

//console.log(dogs);

let cats = [];
// Create Cats 
for (let i = 0; i < 1000; i = i + 1)  {

    let cat = {};
    cat.Name = faker.name.firstName();
    cat.Breed = getAnimal(faker.random.number(), CAT_BREEDS); //Create a list, then % on list.length to get which one
    cat.Sex = getGender(faker.random.number()); //Decide on which each value is

    cat.Shots = faker.random.boolean();
    
    cat.birthDate = faker.date.recent(); //Typically cats will live up to 16 years (small cats at least)
    cat.declawed = faker.random.boolean(); //Check this max age 
    cat.Neutered = faker.random.boolean();
    let ownerNum = getOwnerCount(faker.random.number()); 
    let owners = [];
    for (let i = 0; i < ownerNum; i++) {
        owners.push(faker.random.number() % 1000);
    }
    cat.ownerIds = owners;

    let noteNum = (faker.random.number() % 10); 
    notes = [];
    for (let i = 0; i < noteNum; i++) {
        notes.push(faker.lorem.text());
    }
    cat.NoteIds = notes
    cats.push(cat);
}
//console.log(cats);

let exotics = [];
// Create Exotics 
for (let i = 0; i < 1000; i = i + 1)  {
    let exotic = {};
    exotic.Name = faker.name.firstName();
    exotic.Species = getAnimal(faker.random.number(), EXOTIC_SPECIES); //Create a list, then % on list.length to get which one
    exotic.Sex = getGender(faker.random.number()); 

    exotic.birthDate = faker.date.recent(); 
    exotic.Neutered = faker.random.boolean();
    
    let ownerNum = getOwnerCount(faker.random.number()); 
    let owners = [];
    for (let i = 0; i < ownerNum; i++) {
        owners.push(faker.random.number() % 1000);
    }
    exotic.ownerIds = owners;

    let noteNum = (faker.random.number() % 10); 
    notes = [];
    for (let i = 0; i < noteNum; i++) {
        notes.push(faker.lorem.text());
    }
    exotic.NoteIds = notes
    exotics.push(exotic);
}
//console.log(exotics);  

let owners = [];
// Create Owners 
for (let i = 0; i < 1000; i = i + 1)  {
    let owner = {}
    owner.fName = faker.name.firstName();
    owner.lName = faker.name.lastName();
    owner.addr = faker.address.streetAddress();
    owner.city = faker.address.city();
    owner.state = faker.address.state();
    owner.zip = faker.address.zipCode();
    owner.email = faker.internet.email();
    owner.password = faker.internet.password();
    owners.push(owner);
}

let builtCommands = "";
/*builtCommands += `INSERT INTO owners VALUES\\n`;
for (let owner of owners) {
    builtCommands += `(null, "${owner.fName}", "${owner.lName}", "${owner.addr}", "", "${owner.city}", "${owner.state}", "${owner.zip}", null ),\\n`;
}*/

/*builtCommands += `INSERT INTO cats VALUES\\n`;
for (let cat of cats) {
    let catBDate = `DATE(${cat.birthDate.getFullYear()}/${cat.birthDate.getMonth()}/${cat.birthDate.getDate()})`;
    builtCommands += `( null, "${cat.Name}", "${cat.Breed}", "${cat.Sex}", ${cat.Shots}, ${cat.declawed}, ${cat.Neutered}, ${catBDate} ),\\n`;
}*/

/*builtCommands += `INSERT INTO dogs VALUES\\n`;
for (let dog of dogs) {
    let dogLDate = `DATE(${dog.Liecensed.getFullYear()}/${dog.Liecensed.getMonth()}/${dog.Liecensed.getDate()})`;
    let dogBDate = `DATE(${dog.birthDate.getFullYear()}/${dog.birthDate.getMonth()}/${dog.birthDate.getDate()})`;
    builtCommands += `( null, "${dog.Name}", "${dog.Breed}", "${dog.Sex}", ${dog.Shots}, ${dogLDate}, ${dog.Neutered}, ${dogBDate}, ${dog.Size} ),\\n`;
}*/

builtCommands += `INSERT INTO exotics VALUES\\n`;
for (let exotic of exotics) {
    let exoticBDate = `DATE(${exotic.birthDate.getFullYear()}/${exotic.birthDate.getMonth()}/${exotic.birthDate.getDate()})`;
    builtCommands += `( null, "${exotic.Name}", "${exotic.Species}", "${exotic.Sex}", ${exotic.Neutered}, ${exoticBDate} ),\\n`;
}

var myDiv = document.getElementById('main');
myDiv.innerHTML += builtCommands;

/*
let checkOwners = [];
for (let i = 0; i < 1000; i++) {
    checkOwners[i] = false;
}

for (let cat of cats) {
    for (let owner of cat.ownerIds) {
        checkOwners[owner] = true;
    }
}

for (let dog of dogs) {
    for (let owner of dog.ownerIds) {
        checkOwners[owner] = true;
    }
}

for (let exotic of exotics) {
    for (let owner of exotic.ownerIds) {
        checkOwners[owner] = true;
    }
}

console.log(checkOwners);
console.log(checkOwners.reduce((accum, val) => accum + (val ? 1 : 0)));*/