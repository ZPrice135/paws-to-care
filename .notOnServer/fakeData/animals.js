const DOG_BREEDS = [
    "ENGLISH POINTER",
    "ENGLISH SETTER",
    "KERRY BLUE TERRIER",
    "CAIRN TERRIER",
    "ENGLISH COCKER SPANIEL",
    "GORDON SETTER",
    "AIREDALE TERRIER",
    "AUSTRALIAN TERRIER",
    "BEDLINGTON TERRIER",
    "BORDER TERRIER",
    "BULL TERRIER",
    "FOX TERRIER (SMOOTH)",
    "ENGLISH TOY TERRIER (BLACK &TAN)",
    "SWEDISH VALLHUND",
    "BELGIAN SHEPHERD DOG",
    "OLD ENGLISH SHEEPDOG",
    "GRIFFON NIVERNAIS",
    "BRIQUET GRIFFON VENDEEN",
    "ARIEGEOIS",
    "GASCON SAINTONGEOIS",
    "GREAT GASCONY BLUE",
    "POITEVIN",
    "BILLY",
    "ARTOIS HOUND",
    "PORCELAINE",
    "SMALL BLUE GASCONY BLUE",
    "BLUE GASCONY GRIFFON",
    "GRAND BASSET GRIFFON VENDEEN",
    "NORMAN ARTESIEN BASSET",
    "BLUE GASCONY BASSET",
    "BASSET FAUVE DE BRETAGNE",
    "PORTUGUESE WATER DOG",
    "WELSH CORGI CARDIGAN",
    "WELSH CORGI PEMBROKE",
    "IRISH SOFT COATED WHEATEN TERRIER",
    "YUGOSLAVIAN SHEPHERD DOG - SHARPLANINA",
    "JÄMTHUND",
    "BASENJI",
    "BERGER DE BEAUCE",
    "BERNESE MOUNTAIN DOG",
    "APPENZELL CATTLE DOG",
    "ENTLEBUCH CATTLE DOG",
    "KARELIAN BEAR DOG",
    "FINNISH SPITZ",
    "NEWFOUNDLAND",
    "FINNISH HOUND",
    "POLISH HOUND",
    "KOMONDOR",
    "KUVASZ",
    "PULI",
    "PUMI",
    "HUNGARIAN SHORT-HAIRED POINTER (VIZSLA)",
    "GREAT SWISS MOUNTAIN DOG",
    "SWISS HOUND",
    "SMALL SWISS HOUND",
    "ST. BERNARD",
    "COARSE-HAIRED STYRIAN HOUND",
    "AUSTRIAN BLACK AND TAN HOUND",
    "AUSTRIAN  PINSCHER",
    "MALTESE",
    "FAWN BRITTANY GRIFFON",
    "PETIT BASSET GRIFFON VENDEEN",
    "TYROLEAN HOUND",
    "LAKELAND TERRIER",
    "MANCHESTER TERRIER",
    "NORWICH TERRIER",
    "SCOTTISH TERRIER",
    "SEALYHAM TERRIER",
    "SKYE TERRIER",
    "STAFFORDSHIRE BULL TERRIER",
    "CONTINENTAL TOY SPANIEL",
    "WELSH TERRIER",
    "GRIFFON BRUXELLOIS",
    "GRIFFON BELGE",
    "PETIT BRABANÇON",
    "SCHIPPERKE",
    "BLOODHOUND",
    "WEST HIGHLAND WHITE TERRIER",
    "YORKSHIRE TERRIER",
    "CATALAN SHEEPDOG",
    "SHETLAND SHEEPDOG",
    "IBIZAN PODENCO",
    "BURGOS POINTING DOG",
    "SPANISH MASTIFF",
    "PYRENEAN MASTIFF",
    "PORTUGUESE SHEEPDOG",
    "PORTUGUESE WARREN HOUND-PORTUGUESE PODENGO",
    "BRITTANY SPANIEL",
    "RAFEIRO OF ALENTEJO",
    "GERMAN SPITZ",
    "GERMAN WIRE- HAIRED POINTING DOG",
    "WEIMARANER",
    "WESTPHALIAN DACHSBRACKE",
    "FRENCH BULLDOG",
    "KLEINER MÜNSTERLÄNDER",
    "GERMAN HUNTING TERRIER",
    "GERMAN SPANIEL",
    "FRENCH WATER DOG",
    "BLUE PICARDY SPANIEL",
    "WIRE-HAIRED POINTING GRIFFON KORTHALS",
    "PICARDY SPANIEL",
    "CLUMBER SPANIEL",
    "CURLY COATED RETRIEVER",
    "GOLDEN RETRIEVER",
    "BRIARD",
    "PONT-AUDEMER SPANIEL",
    "SAINT GERMAIN POINTER",
    "DOGUE DE BORDEAUX",
    "DEUTSCH LANGHAAR",
    "LARGE MUNSTERLANDER",
    "GERMAN SHORT- HAIRED POINTING DOG",
    "IRISH RED SETTER",
    "FLAT COATED RETRIEVER",
    "LABRADOR RETRIEVER",
    "FIELD SPANIEL",
    "IRISH WATER SPANIEL",
    "ENGLISH SPRINGER SPANIEL",
    "WELSH SPRINGER SPANIEL",
    "SUSSEX SPANIEL",
    "KING CHARLES SPANIEL",
    "SMÅLANDSSTÖVARE",
    "DREVER",
    "SCHILLERSTÖVARE",
    "HAMILTONSTÖVARE",
    "FRENCH POINTING DOG - GASCOGNE TYPE",
    "FRENCH POINTING DOG - PYRENEAN TYPE",
    "SWEDISH LAPPHUND",
    "CAVALIER KING CHARLES SPANIEL",
    "PYRENEAN MOUNTAIN DOG",
    "PYRENEAN SHEEPDOG - SMOOTH FACED",
    "IRISH TERRIER",
    "BOSTON TERRIER",
    "LONG-HAIRED PYRENEAN SHEEPDOG",
    "SLOVAKIAN CHUVACH",
    "DOBERMANN",
    "BOXER",
    "LEONBERGER",
    "RHODESIAN RIDGEBACK",
    "ROTTWEILER",
    "DACHSHUND",
    "BULLDOG",
    "SERBIAN HOUND",
    "ISTRIAN SHORT-HAIRED HOUND",
    "ISTRIAN WIRE-HAIRED HOUND",
    "DALMATIAN",
    "POSAVATZ HOUND",
    "BOSNIAN BROKEN-HAIRED HOUND - CALLED BARAK",
    "COLLIE ROUGH",
    "BULLMASTIFF",
    "GREYHOUND",
    "ENGLISH FOXHOUND",
    "IRISH WOLFHOUND",
    "BEAGLE",
    "WHIPPET",
    "BASSET HOUND",
    "DEERHOUND",
    "ITALIAN SPINONE",
    "GERMAN SHEPHERD DOG",
    "AMERICAN COCKER SPANIEL",
    "DANDIE DINMONT TERRIER",
    "FOX TERRIER (WIRE)",
    "CASTRO LABOREIRO DOG",
    "BOUVIER DES ARDENNES",
    "POODLE",
    "ESTRELA MOUNTAIN DOG",
    "FRENCH SPANIEL",
    "PICARDY SHEEPDOG",
    "ARIEGE POINTING DOG",
    "BOURBONNAIS POINTING DOG",
    "AUVERGNE POINTER",
    "GIANT SCHNAUZER",
    "SCHNAUZER",
    "MINIATURE SCHNAUZER",
    "GERMAN PINSCHER",
    "MINIATURE PINSCHER",
    "AFFENPINSCHER",
    "PORTUGUESE POINTING DOG",
    "SLOUGHI",
    "FINNISH LAPPHUND",
    "HOVAWART",
    "BOUVIER DES FLANDRES",
    "KROMFOHRLÄNDER",
    "BORZOI - RUSSIAN HUNTING SIGHTHOUND",
    "BERGAMASCO SHEPHERD DOG",
    "ITALIAN VOLPINO",
    "BOLOGNESE",
    "NEAPOLITAN MASTIFF",
    "ITALIAN ROUGH-HAIRED SEGUGIO",
    "CIRNECO DELL'ETNA",
    "ITALIAN GREYHOUND",
    "MAREMMA AND THE ABRUZZES SHEEPDOG",
    "ITALIAN POINTING DOG",
    "NORWEGIAN HOUND",
    "SPANISH HOUND",
    "CHOW CHOW",
    "JAPANESE CHIN",
    "PEKINGESE",
    "SHIH TZU",
    "TIBETAN TERRIER",
    "SAMOYED",
    "HANOVERIAN SCENTHOUND",
    "HELLENIC HOUND",
    "BICHON FRISE",
    "PUDELPOINTER",
    "BAVARIAN MOUNTAIN SCENT HOUND",
    "CHIHUAHUA",
    "FRENCH TRICOLOUR HOUND",
    "FRENCH WHITE & BLACK HOUND",
    "FRISIAN WATER DOG",
    "STABIJHOUN",
    "DUTCH SHEPHERD DOG",
    "DRENTSCHE PARTRIDGE DOG",
    "FILA BRASILEIRO",
    "LANDSEER (EUROPEAN CONTINENTAL TYPE)",
    "LHASA APSO",
    "AFGHAN HOUND",
    "SERBIAN TRICOLOUR HOUND",
    "TIBETAN MASTIFF",
    "TIBETAN SPANIEL",
    "DEUTSCH STICHELHAAR",
    "LITTLE LION DOG",
    "XOLOITZCUINTLE",
    "GREAT DANE",
    "AUSTRALIAN SILKY TERRIER",
    "NORWEGIAN BUHUND",
    "MUDI",
    "HUNGARIAN WIRE-HAIRED POINTER",
    "HUNGARIAN GREYHOUND",
    "HUNGARIAN HOUND - TRANSYLVANIAN SCENT HOUND",
    "NORWEGIAN ELKHOUND GREY",
    "ALASKAN MALAMUTE",
    "SLOVAKIAN HOUND",
    "BOHEMIAN WIRE-HAIRED POINTING GRIFFON",
    "CESKY TERRIER",
    "ATLAS MOUNTAIN DOG (AIDI)",
    "PHARAOH HOUND",
    "MAJORCA MASTIFF",
    "HAVANESE",
    "POLISH LOWLAND SHEEPDOG",
    "TATRA SHEPHERD DOG",
    "PUG",
    "ALPINE DACHSBRACKE",
    "AKITA",
    "SHIBA",
    "JAPANESE TERRIER",
    "TOSA",
    "HOKKAIDO",
    "JAPANESE SPITZ",
    "CHESAPEAKE BAY RETRIEVER",
    "MASTIFF",
    "NORWEGIAN LUNDEHUND",
    "HYGEN HOUND",
    "HALDEN HOUND",
    "NORWEGIAN ELKHOUND BLACK",
    "SALUKI",
    "SIBERIAN HUSKY",
    "BEARDED COLLIE",
    "NORFOLK TERRIER",
    "CANAAN DOG",
    "GREENLAND DOG",
    "NORRBOTTENSPITZ",
    "CROATIAN SHEPHERD DOG",
    "KARST SHEPHERD DOG",
    "MONTENEGRIN MOUNTAIN HOUND",
    "OLD DANISH POINTING DOG",
    "GRAND GRIFFON VENDEEN",
    "COTON DE TULEAR",
    "LAPPONIAN HERDER",
    "SPANISH GREYHOUND",
    "AMERICAN STAFFORDSHIRE TERRIER",
    "AUSTRALIAN CATTLE DOG",
    "CHINESE CRESTED DOG",
    "ICELANDIC SHEEPDOG",
    "BEAGLE HARRIER",
    "EURASIAN",
    "DOGO ARGENTINO",
    "AUSTRALIAN KELPIE",
    "OTTERHOUND",
    "HARRIER",
    "COLLIE SMOOTH",
    "BORDER COLLIE",
    "ROMAGNA WATER DOG",
    "GERMAN HOUND",
    "BLACK AND TAN COONHOUND",
    "AMERICAN WATER SPANIEL",
    "IRISH GLEN OF IMAAL TERRIER",
    "AMERICAN FOXHOUND",
    "RUSSIAN-EUROPEAN LAIKA",
    "EAST SIBERIAN LAIKA",
    "WEST SIBERIAN LAIKA",
    "AZAWAKH",
    "DUTCH SMOUSHOND",
    "SHAR PEI",
    "PERUVIAN HAIRLESS DOG",
    "SAARLOOS WOLFHOND",
    "NOVA SCOTIA DUCK TOLLING RETRIEVER",
    "DUTCH SCHAPENDOES",
    "NEDERLANDSE KOOIKERHONDJE",
    "BROHOLMER",
    "FRENCH WHITE AND ORANGE HOUND",
    "KAI",
    "KISHU",
    "SHIKOKU",
    "WIREHAIRED SLOVAKIAN POINTER",
    "MAJORCA SHEPHERD DOG",
    "GREAT ANGLO-FRENCH TRICOLOUR HOUND",
    "GREAT ANGLO-FRENCH WHITE AND BLACK HOUND",
    "GREAT ANGLO-FRENCH WHITE & ORANGE HOUND",
    "MEDIUM-SIZED ANGLO-FRENCH HOUND",
    "SOUTH RUSSIAN SHEPHERD DOG",
    "RUSSIAN BLACK TERRIER",
    "CAUCASIAN SHEPHERD DOG",
    "CANARIAN WARREN HOUND",
    "IRISH RED AND WHITE SETTER",
    "ANATOLIAN SHEPHERD DOG",
    "CZECHOSLOVAKIAN WOLFDOG",
    "POLISH GREYHOUND",
    "KOREA JINDO DOG",
    "CENTRAL ASIA SHEPHERD DOG",
    "SPANISH WATER DOG",
    "ITALIAN SHORT-HAIRED SEGUGIO",
    "THAI RIDGEBACK DOG",
    "PARSON RUSSELL TERRIER",
    "SAINT MIGUEL CATTLE DOG",
    "BRAZILIAN TERRIER",
    "AUSTRALIAN SHEPHERD",
    "ITALIAN CORSO DOG",
    "AMERICAN AKITA",
    "JACK RUSSELL TERRIER",
    "DOGO CANARIO",
    "WHITE SWISS SHEPHERD DOG",
    "TAIWAN DOG",
    "ROMANIAN MIORITIC SHEPHERD DOG",
    "ROMANIAN CARPATHIAN SHEPHERD DOG",
    "AUSTRALIAN STUMPY TAIL CATTLE DOG",
    "RUSSIAN TOY",
    "CIMARRÓN URUGUAYO",
    "POLISH HUNTING DOG",
    "BOSNIAN AND HERZEGOVINIAN - CROATIAN SHEPHERD DOG",
    "DANISH-SWEDISH FARMDOG",
    "SOUTHEASTERN EUROPEAN SHEPHERD",
    "THAI BANGKAEW DOG",
    "MINIATURE BULL TERRIER",
    "LANCASHIRE HEELER",
];

const CAT_BREEDS = [
    "Abyssinian",
    "American Bobtail",
    "American Curl",
    "American Shorthair",
    "American Wirehair",
    "Balinese",
    "Bengal",
    "Birman",
    "Bombay",
    "British Shorthair",
    "Burmese",
    "Chartreux",
    "Cornish Rex",
    "Cymric",
    "Devon Rex",
    "Egyptian Mau",
    "Exotic Shorthair",
    "Havana Brown",
    "Himalayan",
    "Japanese Bobtail",
    "Javanese",
    "Korat ",
    "Maine Coon",
    "Manx",
    "Munchkin",
    "Nebelung",
    "Norwegian Forest Cat",
    "Ocicat",
    "Oriental",
    "Persian",
    "Ragdoll",
    "Russian Blue",
    "Scottish Fold",
    "Selkirk Rex",
    "Siamese",
    "Siberian",
    "Singapura",
    "Snowshoe",
    "Somali",
    "Sphynx",
    "Tonkinese",
    "Turkish Angora",
    "Turkish Van",    
];

const EXOTIC_SPECIES = [
    "Canidae",
    "Felidae",
    "Cattle",
    "Donkey",
    "Goat",
    "Guinea pig",
    "Horse",
    "Pig",
    "Rabbit",
    "Fancy rat varieties",
    "laboratory rat strains",
    "Sheep breeds",
    "Water buffalo breeds",
    "Chicken breeds",
    "Duck breeds",
    "Goose breeds",
    "Pigeon breeds",
    "Turkey breeds",
    "Aardvark",
    "Aardwolf",
    "African buffalo",
    "African elephant",
    "African leopard",
    "Albatross",
    "Alligator",
    "Alpaca",
    "American buffalo (bison)",
    "American robin",
    "Amphibian",
    "list",
    "Anaconda",
    "Angelfish",
    "Anglerfish",
    "Ant",
    "Anteater",
    "Antelope",
    "Antlion",
    "Ape",
    "Aphid",
    "Arabian leopard",
    "Arctic Fox",
    "Arctic Wolf",
    "Armadillo",
    "Arrow crab",
    "Asp",
    "Ass (donkey)",
    "Baboon",
    "Badger",
    "Bald eagle",
    "Bandicoot",
    "Barnacle",
    "Barracuda",
    "Basilisk",
    "Bass",
    "Bat",
    "Beaked whale",
    "Bear",
    "list",
    "Beaver",
    "Bedbug",
    "Bee",
    "Beetle",
    "Bird",
    "list",
    "Bison",
    "Blackbird",
    "Black panther",
    "Black widow spider",
    "Blue bird",
    "Blue jay",
    "Blue whale",
    "Boa",
    "Boar",
    "Bobcat",
    "Bobolink",
    "Bonobo",
    "Booby",
    "Box jellyfish",
    "Bovid",
    "Buffalo, African",
    "Buffalo, American (bison)",
    "Bug",
    "Butterfly",
    "Buzzard",
    "Camel",
    "Canid",
    "Cape buffalo",
    "Capybara",
    "Cardinal",
    "Caribou",
    "Carp",
    "Cat",
    "list",
    "Catshark",
    "Caterpillar",
    "Catfish",
    "Cattle",
    "list",
    "Centipede",
    "Cephalopod",
    "Chameleon",
    "Cheetah",
    "Chickadee",
    "Chicken",
    "list",
    "Chimpanzee",
    "Chinchilla",
    "Chipmunk",
    "Clam",
    "Clownfish",
    "Cobra",
    "Cockroach",
    "Cod",
    "Condor",
    "Constrictor",
    "Coral",
    "Cougar",
    "Cow",
    "Coyote",
    "Crab",
    "Crane",
    "Crane fly",
    "Crawdad",
    "Crayfish",
    "Cricket",
    "Crocodile",
    "Crow",
    "Cuckoo",
    "Cicada",
    "Damselfly",
    "Deer",
    "Dingo",
    "Dinosaur",
    "list",
    "Dog",
    "list",
    "Dolphin",
    "Donkey",
    "list",
    "Dormouse",
    "Dove",
    "Dragonfly",
    "Dragon",
    "Duck",
    "list",
    "Dung beetle",
    "Eagle",
    "Earthworm",
    "Earwig",
    "Echidna",
    "Eel",
    "Egret",
    "Elephant",
    "Elephant seal",
    "Elk",
    "Emu",
    "English pointer",
    "Ermine",
    "Falcon",
    "Ferret",
    "Finch",
    "Firefly",
    "Fish",
    "Flamingo",
    "Flea",
    "Fly",
    "Flyingfish",
    "Fowl",
    "Fox",
    "Frog",
    "Fruit bat",
    "Gamefowl",
    "list",
    "Galliform",
    "list",
    "Gazelle",
    "Gecko",
    "Gerbil",
    "Giant panda",
    "Giant squid",
    "Gibbon",
    "Gila monster",
    "Giraffe",
    "Goat",
    "list",
    "Goldfish",
    "Goose",
    "list",
    "Gopher",
    "Gorilla",
    "Grasshopper",
    "Great blue heron",
    "Great white shark",
    "Grizzly bear",
    "Ground shark",
    "Ground sloth",
    "Grouse",
    "Guan",
    "list",
    "Guanaco",
    "Guineafowl",
    "list",
    "Guinea pig",
    "list",
    "Gull",
    "Guppy",
    "Haddock",
    "Halibut",
    "Hammerhead shark",
    "Hamster",
    "Hare",
    "Harrier",
    "Hawk",
    "Hedgehog",
    "Hermit crab",
    "Heron",
    "Herring",
    "Hippopotamus",
    "Hookworm",
    "Hornet",
    "Horse",
    "list",
    "Hoverfly",
    "Hummingbird",
    "Humpback whale",
    "Hyena",
    "Iguana",
    "Impala",
    "Irukandji jellyfish",
    "Jackal",
    "Jaguar",
    "Jay",
    "Jellyfish",
    "Junglefowl",
    "Kangaroo",
    "Kangaroo mouse",
    "Kangaroo rat",
    "Kingfisher",
    "Kite",
    "Kiwi",
    "Koala",
    "Koi",
    "Komodo dragon",
    "Krill",
    "Ladybug",
    "Lamprey",
    "Landfowl",
    "Land snail",
    "Lark",
    "Leech",
    "Lemming",
    "Lemur",
    "Leopard",
    "Leopon",
    "Limpet",
    "Lion",
    "Lizard",
    "Llama",
    "Lobster",
    "Locust",
    "Loon",
    "Louse",
    "Lungfish",
    "Lynx",
    "Macaw",
    "Mackerel",
    "Magpie",
    "Mammal",
    "Manatee",
    "Mandrill",
    "Manta ray",
    "Marlin",
    "Marmoset",
    "Marmot",
    "Marsupial",
    "Marten",
    "Mastodon",
    "Meadowlark",
    "Meerkat",
    "Mink",
    "Minnow",
    "Mite",
    "Mockingbird",
    "Mole",
    "Mollusk",
    "Mongoose",
    "Monitor lizard",
    "Monkey",
    "Moose",
    "Mosquito",
    "Moth",
    "Mountain goat",
    "Mouse",
    "Mule",
    "Muskox",
    "Narwhal",
    "Newt",
    "New World quail",
    "Nightingale",
    "Ocelot",
    "Octopus",
    "Old World quail",
    "Opossum",
    "Orangutan",
    "Orca",
    "Ostrich",
    "Otter",
    "Owl",
    "Ox",
    "Panda",
    "Panther",
    "Panthera hybrid",
    "Parakeet",
    "Parrot",
    "Parrotfish",
    "Partridge",
    "Peacock",
    "Peafowl",
    "Pelican",
    "Penguin",
    "Perch",
    "Peregrine falcon",
    "Pheasant",
    "Pig",
    "Pigeon",
    "list",
    "Pike",
    "Pilot whale",
    "Pinniped",
    "Piranha",
    "Planarian",
    "Platypus",
    "Polar bear",
    "Pony",
    "Porcupine",
    "Porpoise",
    "Portuguese man o' war",
    "Possum",
    "Prairie dog",
    "Prawn",
    "Praying mantis",
    "Primate",
    "Ptarmigan",
    "Puffin",
    "Puma",
    "Python",
    "Quail",
    "Quelea",
    "Quokka",
    "Rabbit",
    "list",
    "Raccoon",
    "Rainbow trout",
    "Rat",
    "Rattlesnake",
    "Raven",
    "Ray (Batoidea)",
    "Ray (Rajiformes)",
    "Red panda",
    "Reindeer",
    "Reptile",
    "Rhinoceros",
    "Right whale",
    "Roadrunner",
    "Rodent",
    "Rook",
    "Rooster",
    "Roundworm",
    "Saber-toothed cat",
    "Sailfish",
    "Salamander",
    "Salmon",
    "Sawfish",
    "Scale insect",
    "Scallop",
    "Scorpion",
    "Seahorse",
    "Sea lion",
    "Sea slug",
    "Sea snail",
    "Shark",
    "list",
    "Sheep",
    "list",
    "Shrew",
    "Shrimp",
    "Silkworm",
    "Silverfish",
    "Skink",
    "Skunk",
    "Sloth",
    "Slug",
    "Smelt",
    "Snail",
    "Snake",
    "list",
    "Snipe",
    "Snow leopard",
    "Sockeye salmon",
    "Sole",
    "Sparrow",
    "Sperm whale",
    "Spider",
    "Spider monkey",
    "Spoonbill",
    "Squid",
    "Squirrel",
    "Starfish",
    "Star-nosed mole",
    "Steelhead trout",
    "Stingray",
    "Stoat",
    "Stork",
    "Sturgeon",
    "Sugar glider",
    "Swallow",
    "Swan",
    "Swift",
    "Swordfish",
    "Swordtail",
    "Tahr",
    "Takin",
    "Tapir",
    "Tarantula",
    "Tarsier",
    "Tasmanian devil",
    "Termite",
    "Tern",
    "Thrush",
    "Tick",
    "Tiger",
    "Tiger shark",
    "Tiglon",
    "Toad",
    "Tortoise",
    "Toucan",
    "Trapdoor spider",
    "Tree frog",
    "Trout",
    "Tuna",
    "Turkey",
    "list",
    "Turtle",
    "Tyrannosaurus",
    "Urial",
    "Vampire bat",
    "Vampire squid",
    "Vicuna",
    "Viper",
    "Vole",
    "Vulture",
    "Wallaby",
    "Walrus",
    "Wasp",
    "Warbler",
    "Water Boa",
    "Water buffalo",
    "Weasel",
    "Whale",
    "Whippet",
    "Whitefish",
    "Whooping crane",
    "Wildcat",
    "Wildebeest",
    "Wildfowl",
    "Wolf",
    "Wolverine",
    "Wombat",
    "Woodpecker",
    "Worm",
    "Wren",
    "Xerinae",
    "X-ray fish",
    "Yak",
    "Yellow perch",
    "Zebra",
    "Zebra finch",
    "Animals by number of neurons",
    "Animals by size",
    "Common household pests",
    "Common names of poisonous animals",
    "Alpaca",
    "Bali cattle",
    "Cat",
    "Cattle",
    "Chicken",
    "Dog",
    "Domestic Bactrian camel",
    "Domestic canary",
    "Domestic dromedary camel",
    "Domestic duck",
    "Domestic goat",
    "Domestic goose",
    "Domestic guineafowl",
    "Domestic hedgehog",
    "Domestic pig",
    "Domestic pigeon",
    "Domestic rabbit",
    "Domestic silkmoth",
    "Domestic silver fox",
    "Domestic turkey",
    "Donkey",
    "Fancy mouse",
    "Fancy rat",
    "Lab rat",
    "Ferret",
    "Gayal",
    "Goldfish",
    "Guinea pig",
    "Guppy",
    "Horse",
    "Koi",
    "Llama",
    "Ringneck dove",
    "Sheep",
    "Siamese fighting fish",
    "Society finch",
    "Yak",
    "Water buffalo",
];