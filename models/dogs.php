<?php 
require_once "../config.php";
require_once "./dogs.model.php";

$id = $_GET["id"] ?? 1; 
$notes = $_GET["notes"] ?? false; 
$owners = $_GET["owners"] ?? false; 
if ($owners) {
  echo json_encode(Dogs::fetchOwners($id));
} else {
  echo json_encode(Dogs::fetchAll());
}