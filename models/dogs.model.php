<?php 
require_once "../config.php";
class Dogs {

  static $headers = [ "Name", "Breed", "Sex", "Shots", "Licensed", "Neutered", "Birthdate", "Weight" ];

  static function fetchById($id) {
    global $db; 
    $query = $db->prepare("SELECT * FROM dogs WHERE id = ?");
    $query->bind_param("i", $id);
    $query->execute();
    $query->bind_result($id, $name, $breed, $sex, $shots, $licensed, $neutered, $birthdate, $size); 
    while($query->fetch()) {
      return[
        "id" => $id,
        "name" => $name, 
        "breed" => $breed, 
        "sex" => $sex, 
        "shots" => $shots, 
        "licensed" => $licensed, 
        "neutered" => $neutered, 
        "birthdate" => $birthdate, 
        "size" => $size
      ]; 
    }
  }

  //Gets cats for specific owner
  static function fetchByOwnerId($ownerId) {
    $dogIds = []; 
    $dogs = [];
    global $db; 
    $query = $db->prepare('SELECT * FROM dogsOwners WHERE ownersFk = ?');
    $query->bind_param("i", $ownerId);
    $query->execute(); 
    $query->bind_result($id, $dogsFk, $ownersFk);

    while($query->fetch()){
      array_push($dogIds, $dogsFk);
    }

    foreach($dogIds as $dogId) {
      array_push($dogs, Dogs::fetchById($dogId));
    }

    return $dogs;
  }

  static function fetchOwners($ownerId) {
    $owners = []; 
    global $db; 
    $query = $db->prepare('SELECT o.fName, o.lName FROM owners o LEFT JOIN dogOwners d WHERE d.dogsFK = ?');
    $query->bind_param("i", $ownerId);
    $query->execute(); 
    $query->bind_result($fName, $lName);

    while($query->fetch()){
      array_push($owners, ["$fName $lName", "Address?"]);
    }

    return $owners;
  }


  static function fetchAll(){
    $data = [];
    global $db; 
    $query = $db->prepare('SELECT * FROM dogs');
    $query->execute(); 
    $query->bind_result($id, $name, $breed, $sex, $shots, $licensed, $neutered, $birthdate, $size);
    
    while($query->fetch()) {
      array_push($data, [
        "id" => $id,
        "name" => $name, 
        "breed" => $breed, 
        "sex" => $sex, 
        "shots" => $shots, 
        "licensed" => $licensed, 
        "neutered" => $neutered, 
        "birthdate" => $birthdate, 
        "size" => $size
      ]); 
    }

    return $data;
  }
}