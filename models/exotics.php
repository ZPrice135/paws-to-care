<?php 
require_once "../config.php";
require_once "./exotics.model.php";

$id = $_GET["id"] ?? 1; 
$notes = $_GET["notes"] ?? false; 
$owners = $_GET["owners"] ?? false; 
if ($owners) {
  echo json_encode(Exotics::fetchOwners($id));
} else {
  echo json_encode(Exotics::fetchAll());
}