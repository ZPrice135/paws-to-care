<?php 
require_once "../config.php";

class Cats {

  static $headers = ["Name", "Breed", "Sex", "Shots", "Declawed", "Neutered", "Birthdate"];

  static function fetchById($id) {
    global $db; 
    $query = $db->prepare("SELECT * FROM cats WHERE id = ?");
    $query->bind_param("i", $id);
    $query->execute();
    $query->bind_result($id, $name, $breed, $sex, $shots, $declawed, $neutered, $birthdate); 
    while($query->fetch()) {
      return[
        "id" => $id, 
        "name" => $name, 
        "breed" => $breed, 
        "sex" => $sex, 
        "shots" =>  $shots, 
        "declawed" => $declawed, 
        "neutered" => $neutered, 
        "birthdate" => $birthdate
      ];
    }
  }

  //Gets cats for specific owner
  static function fetchByOwnerId($ownerId) {
    $catIds = []; 
    $cats = [];
    global $db; 
    $query = $db->prepare('SELECT * FROM catsOwners WHERE ownersFk = ?');
    $query->bind_param("i", $ownerId);
    $query->execute(); 
    $query->bind_result($id, $catsFk, $ownersFk);

    while($query->fetch()){
      array_push($catIds, $catsFk);
    }

    foreach($catIds as $catId) {
      array_push($cats, Cats::fetchById($catId));
    }

    return $cats;
  }

  static function fetchOwners($ownerId) {
    $owners = []; 
    global $db; 
    $query = $db->prepare('SELECT o.fName, o.lName FROM owners o LEFT JOIN catOwners c WHERE co.catsFK = ?');
    $query->bind_param("i", $ownerId);
    $query->execute(); 
    $query->bind_result($fName, $lName);

    while($query->fetch()){
      array_push($owners, ["$fName $lName", "Address?"]);
    }

    return $owners;
  }

  static function fetchAll(){
    $data = [];
    global $db; 
    $query = $db->prepare('SELECT * FROM cats');
    $query->execute(); 
    $query->bind_result($id, $name, $breed, $sex, $shots, $declawed, $neutered, $birthday);
    
    while($query->fetch()) {
      $tempArr = ["id" => $id, "name"=>$name, "breed"=>$breed, "sex"=>$sex, "shots"=>$shots, "declawed"=>$declawed, "neutered"=>$neutered, "birthday"=>$birthday];
      array_push($data, $tempArr); 
    }

    return $data;
  }
}
