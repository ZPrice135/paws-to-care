<?php 
require_once "../config.php";
require_once "./cats.model.php";

$id = $_GET["id"] ?? 1; 
$notes = $_GET["notes"] ?? false; 
$owners = $_GET["owners"] ?? false; 
if ($owners) {
  echo json_encode(Cats::fetchOwners($id));
} else {
  echo json_encode(Cats::fetchAll());
}

?>