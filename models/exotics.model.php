<?php 
require_once "../config.php";
class Exotics {

  static $headers = ["Name", "Breed", "Sex", "Shots", "Declawed", "Neutered", "Birthdate"];

  static function fetchById($id) {
    global $db; 
    $query = $db->prepare("SELECT * FROM exotics WHERE id = ?");
    $query->bind_param("i", $id);
    $query->execute();
    $query->bind_result($id, $name, $species, $sex, $neutered, $birthdate); 
    while($query->fetch()) {
      return[
        "id" => $id, 
        "name" => $name, 
        "species" => $species, 
        "sex" => $sex, 
        "neutered" => $neutered, 
        "birthdate" => $birthdate
      ];
    }
  }

  //Gets exotics for specific owner
  static function fetchByOwnerId($ownerId) {
    $exoticIds = []; 
    $exotics = [];
    global $db; 
    $query = $db->prepare('SELECT * FROM exoticsOwners WHERE ownersFk = ?');
    $query->bind_param("i", $ownerId);
    $query->execute(); 
    $query->bind_result($id, $exoticsFk, $ownersFk);

    while($query->fetch()){
      array_push($exoticIds, $exoticsFk);
    }

    foreach($exoticIds as $exoticId) {
      array_push($exotics, Exotics::fetchById($exoticId));
    }

    return $exotics;
  }

  static function fetchAll(){
    $data = [];
    global $db; 
    $query = $db->prepare('SELECT * FROM exotics');
    $query->execute(); 
    $query->bind_result($id, $name, $species, $sex, $neutered, $birthdate);
    
    while($query->fetch()) {
      $tempArr = ["id"=> $id, "name"=> $name, "species"=> $species, "sex"=> $sex, "neutered"=> $neutered, "birthdate"=> $birthdate];
      array_push($data, $tempArr); 
    }

    return $data;
  }
}
