<?php
    class Owners {

        static $headers = ["First Name", "Last Name", "Address 1", "Address 2", "City", "Street", "Zip"];

        static function getById($ownerId){
            $data = [];
            global $db;
            $query = $db->prepare('SELECT * FROM owners WHERE id = ?');
            $query->bind_param("i", $ownerId);
            $query->execute();
            $query->bind_result($id, $fname, $lname, $add1, $add2, $city, $st, $zip, $neutered);

            while($query->fetch()){
                $tempArray = [ $fname, $lname, $add1, $add2, $city, $st, $zip ];
                array_push($data, $tempArray);
            }

            return $data;
        }

        static function getAll(){
            $data = [];
            global $db;
            $query = $db->prepare('SELECT * FROM owners');
            $query->execute();
            $query->bind_result($id, $fname, $lname, $add1, $add2, $city, $st, $zip, $neutered);

            while($query->fetch()){
                $tempArray = [ $fname, $lname, $add1, $add2, $city, $st, $zip ];
                array_push($data, $tempArray);
            }
            
            return $data;
        }
    }

?>


