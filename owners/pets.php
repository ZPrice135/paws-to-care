<?php 
include "../templates/header.php" ;
require_once "../config.php";
require_once "../models/cats.model.php";
require_once "../models/dogs.model.php";
require_once "../models/exotics.model.php";

$catObj = new Cats;
$dogObj = new Dogs;
$exoticObj = new Exotics; 
//Cats is an array
$cats = $catObj->fetchByOwnerId($_SESSION["userId"]);
$dogs = $dogObj->fetchByOwnerId($_SESSION["userId"]);
$exotics = $exoticObj->fetchByOwnerId($_SESSION["userId"]);

?>

<div class="container">

    <?php if(count($cats) > 0): ?>
        <div class="row">
            <h1 class="display-4">Cat<?= count($cats)>1 ? "s" : "" ?></h1>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Breed</th>
                            <th>Sex</th>
                            <th>Shots</th>
                            <th>Declawed</th>
                            <th>Neutered</th>
                            <th>Birthdate</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for( $i = 0; $i < count( $cats ); $i++ ): ?>
                            <tr>
                                <td><?= $cats[$i]['name'] ?></td>
                                <td><?= $cats[$i]['breed'] ?></td>
                                <td><?= $cats[$i]['sex'] ?></td>
                                <td><?= $cats[$i]['shots'] ? '<i class="fas fa-check" style="color:green;"></i>' : '<i class="fas fa-times"></i>' ?></td>
                                <td><?= $cats[$i]['declawed'] ? '<i class="fas fa-check"  style="color:green;"></i>' : '<i class="fas fa-times"></i>'   ?></td>
                                <td><?= $cats[$i]['neutered'] ? '<i class="fas fa-check"  style="color:green;"></i>' : '<i class="fas fa-times"></i>'  ?></td>
                                <td><?= date('Y-m-d', strtotime($cats[$i]['birthdate'])); ?></td>
                                <!-- Modal Start -->
                                <td>
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal<?= $cats[$i]['id'] ?>">Notes</button>
                                    <div id="modal<?= $cats[$i]['id'] ?>" class="modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><?= $cats[$i]['name']?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-sm">
                                                            <thead>
                                                                <tr>
                                                                    <td>
                                                                        Date
                                                                    </td>
                                                                    <td>
                                                                        Note
                                                                    </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $m_query = $db->prepare("SELECT n.vetName, n.date, n.note FROM ownernotes n LEFT JOIN owners o ON n.ownersFk = o.id WHERE o.id = ?"); 
                                                                    $m_query->bind_param("s", $cats[$i]['id']); 
                                                                    $m_query->execute();
                                                                    $m_query->bind_result($vetName, $date, $note);
                                                                    while($m_query->fetch()) {
                                                                        echo '<tr><td>'.date('Y-m-d', strtotime($date)).'</td><td>'.$note.'</td></tr>';
                                                                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <!-- Modal End -->
                            </tr> 
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif ?>

    <?php if(count($dogs) > 0): ?>
        <div class="row">
            <h1 class="display-4">Dog<?= (count($dogs)>1) ? "s" : ""?></h1>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Breed</th>
                            <th>Sex</th>
                            <th>Shots</th>
                            <th>Licensed</th>
                            <th>Neutered</th>
                            <th>Birthdate</th>
                            <th>Size</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for( $i = 0; $i < count( $dogs ); $i++ ): ?>
                            <tr>
                                <td><?= $dogs[$i]['name'] ?></td>
                                <td><?= $dogs[$i]['breed'] ?></td>
                                <td><?= $dogs[$i]['sex'] ?></td>
                                <td><?= $dogs[$i]['shots'] ? '<i class="fas fa-check" style="color:green;"></i>' : '<i class="fas fa-times"></i>' ?></td>
                                <td><?= $dogs[$i]['licensed'] ? '<i class="fas fa-check"  style="color:green;"></i>' : '<i class="fas fa-times"></i>'   ?></td>
                                <td><?= $dogs[$i]['neutered'] ? '<i class="fas fa-check"  style="color:green;"></i>' : '<i class="fas fa-times"></i>'  ?></td>
                                <td><?= date('Y-m-d', strtotime($dogs[$i]['birthdate'])); ?></td>
                                <td><?= $dogs[$i]['size'] ?></td>
                                <!-- Modal Start -->
                                <td>
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal<?= $dogs[$i]['id'] ?>">Notes</button>
                                    <div id="modal<?= $dogs[$i]['id'] ?>" class="modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><?= $dogs[$i]['name']?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-sm">
                                                            <thead>
                                                                <tr>
                                                                    <td>
                                                                        Date
                                                                    </td>
                                                                    <td>
                                                                        Note
                                                                    </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $m_query = $db->prepare("SELECT n.vetName, n.date, n.note FROM ownernotes n LEFT JOIN owners o ON n.ownersFk = o.id WHERE o.id = ?"); 
                                                                    $m_query->bind_param("s", $dogs[$i]['id']); 
                                                                    $m_query->execute();
                                                                    $m_query->bind_result($vetName, $date, $note);
                                                                    while($m_query->fetch()) {
                                                                        echo '<tr><td>'.date('Y-m-d', strtotime($date)).'</td><td>'.$note.'</td></tr>';
                                                                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <!-- Modal End -->
                            </tr> 
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif ?>

    <?php if(count($exotics) > 0): ?>
        <div class="row">
            <h1 class="display-4">Exotic<?= (count($exotics)>1)  ? "s" : "" ?></h1>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Species</th>
                            <th>Sex</th>
                            <th>Neutered</th>
                            <th>Birthdate</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for( $i = 0; $i < count( $exotics ); $i++ ): ?>
                            <tr>
                                <td><?= $exotics[$i]['name'] ?></td>
                                <td><?= $exotics[$i]['species'] ?></td>
                                <td><?= $exotics[$i]['sex'] ?></td>
                                <td><?= $exotics[$i]['neutered'] ? '<i class="fas fa-check"  style="color:green;"></i>' : '<i class="fas fa-times"></i>'  ?></td>
                                <td><?= date('Y-m-d', strtotime($exotics[$i]['birthdate'])); ?></td>
                                <!-- Modal Start -->
                                <td>
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal<?= $exotics[$i]['id'] ?>">Notes</button>
                                    <div id="modal<?= $exotics[$i]['id'] ?>" class="modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><?= $exotics[$i]['name']?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-sm">
                                                            <thead>
                                                                <tr>
                                                                    <td>
                                                                        Date
                                                                    </td>
                                                                    <td>
                                                                        Note
                                                                    </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $m_query = $db->prepare("SELECT n.vetName, n.date, n.note FROM ownernotes n LEFT JOIN owners o ON n.ownersFk = o.id WHERE o.id = ?"); 
                                                                    $m_query->bind_param("s", $exotics[$i]['id']); 
                                                                    $m_query->execute();
                                                                    $m_query->bind_result($vetName, $date, $note);
                                                                    while($m_query->fetch()) {
                                                                        echo '<tr><td>'.date('Y-m-d', strtotime($date)).'</td><td>'.$note.'</td></tr>';
                                                                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <!-- Modal End -->
                            </tr> 
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif ?>
</div>



<?php include "../templates/footer.php" ?>