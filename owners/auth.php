<?php
if( !$_SESSION['userId'] &&
    !$_SESSION["username"] &&
    $_SESSION["userType"] !== "owner" )
    {
        header("Location: /");
        die;
    }
?>