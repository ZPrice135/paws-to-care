<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <link rel="stylesheet" href="http://pagination.js.org/dist/2.0.7/pagination.css" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js" crossorigin="anonymous"></script>
  <script src="http://pagination.js.org/dist/2.1.3/pagination.min.js" crossorigin="anonymous"></script>
  <script src="/js/adminScripts.js"></script>
  <style>
    .pagination > li > a, 
    .pagination > li > span {
      color: #343A40;
    } 

    .pagination > li > a:hover, 
    .pagination > li > span:hover {
      color: #000000;
    }

    .pagination > li > a:target, 
    .pagination > li > span:target,
    .pagination > li > a:focus, 
    .pagination > li > span:focus {
      box-shadow: 0 0 0 3px #9DA5AF;
    }

    .pagination > li.active > a, 
    .pagination > li.active >span {
      background-color: #343a40!important; 
      border: 1px solid #343a40!important; 
      color: #FFFFFF;
    }

    .input-group > .form-control:target,
    .input-group > .form-control:focus {
      box-shadow: 0 0 0 3px #B5BABE!important;
    }






  </style>
  <title>Paws to Care</title>
  
  <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/util.php';?>
</head>
<body>
  <?php require_once "navigation.php" ?>
