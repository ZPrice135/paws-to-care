<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top"> 
    <a class="navbar-brand" href="/">Paws to Care</a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="/about">About <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="/contact">Contact <span class="sr-only">(current)</span></a>
        </li>
        <?php if(isLoggedIn()): ?>
            <?php if(isAdmin()): ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/admin/cats">Cats<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/admin/dogs">Dogs<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/admin/exotics">Exotics<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/admin/owners">Owners<span class="sr-only">(current)</span></a>
                </li>
            <?php else: ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/owners/pets.php"><span>My Pets</span></a>
                </li>
            <?php endif ?>
            
            <li class="nav-item active">
                <a class="nav-link" href="/logout">Log Out<span class="sr-only">(current)</span></a>
            </li>

        <?php else: ?>
            <li class="nav-item active">
                <a class="nav-link" href="/login">Log In<span class="sr-only">(current)</span></a>
            </li>
        <?php endif ?>
      </ul>
    </div>
  </nav>