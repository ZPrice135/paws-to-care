<?php 
    include './templates/header.php'; 
    
    if(isLoggedIn()){
        header("Location: /");
        die;
    }
    $loginFail = false;
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $_SESSION = array();
        // Get values submitted from the login form
        $username = $_POST["username"] ?? "";
        $password = $_POST["password"] ?? "";
                
        $result = login($username, $password);
        $loginFail = true;
    }
?>

<div class="container">
  <h1 class="display-4">Login</h1>
  <div class="row">
    <div class="col-lg-5 col-xs-12">
        <form action="" method="post"> 
            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control" name="username" id="username">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <button class="btn" type="submit">Submit</button>
            <?php if($loginFail): ?>
                <span class="text-danger">LOGIN FAILED</span>
            <?php endif; ?>
        </form> 
    </div>
  </div>  
</div>

<?php include './templates/footer.php'; ?>